﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.RB_Tercera = New System.Windows.Forms.RadioButton()
        Me.RB_Segunda = New System.Windows.Forms.RadioButton()
        Me.RB_Primera = New System.Windows.Forms.RadioButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.CB_pelicula = New System.Windows.Forms.CheckBox()
        Me.CB_bebida = New System.Windows.Forms.CheckBox()
        Me.CB_comida = New System.Windows.Forms.CheckBox()
        Me.num_pelicula = New System.Windows.Forms.NumericUpDown()
        Me.num_bebida = New System.Windows.Forms.NumericUpDown()
        Me.num_comida = New System.Windows.Forms.NumericUpDown()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.num_pelicula, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.num_bebida, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.num_comida, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.RB_Tercera)
        Me.GroupBox1.Controls.Add(Me.RB_Segunda)
        Me.GroupBox1.Controls.Add(Me.RB_Primera)
        Me.GroupBox1.Location = New System.Drawing.Point(76, 128)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(186, 239)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Clase de vuelo"
        '
        'RB_Tercera
        '
        Me.RB_Tercera.AutoSize = True
        Me.RB_Tercera.Location = New System.Drawing.Point(28, 149)
        Me.RB_Tercera.Name = "RB_Tercera"
        Me.RB_Tercera.Size = New System.Drawing.Size(118, 21)
        Me.RB_Tercera.TabIndex = 3
        Me.RB_Tercera.TabStop = True
        Me.RB_Tercera.Text = "Tercera Clase"
        Me.RB_Tercera.UseVisualStyleBackColor = True
        '
        'RB_Segunda
        '
        Me.RB_Segunda.AutoSize = True
        Me.RB_Segunda.Location = New System.Drawing.Point(28, 96)
        Me.RB_Segunda.Name = "RB_Segunda"
        Me.RB_Segunda.Size = New System.Drawing.Size(125, 21)
        Me.RB_Segunda.TabIndex = 2
        Me.RB_Segunda.TabStop = True
        Me.RB_Segunda.Text = "Segunda Clase"
        Me.RB_Segunda.UseVisualStyleBackColor = True
        '
        'RB_Primera
        '
        Me.RB_Primera.AutoSize = True
        Me.RB_Primera.Location = New System.Drawing.Point(28, 46)
        Me.RB_Primera.Name = "RB_Primera"
        Me.RB_Primera.Size = New System.Drawing.Size(117, 21)
        Me.RB_Primera.TabIndex = 1
        Me.RB_Primera.TabStop = True
        Me.RB_Primera.Text = "Primera Clase"
        Me.RB_Primera.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("MS Reference Sans Serif", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(70, 50)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(561, 35)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "ELIGA LOS DETALLES DE SU VUELO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.CB_pelicula)
        Me.GroupBox2.Controls.Add(Me.CB_bebida)
        Me.GroupBox2.Controls.Add(Me.CB_comida)
        Me.GroupBox2.Controls.Add(Me.num_pelicula)
        Me.GroupBox2.Controls.Add(Me.num_bebida)
        Me.GroupBox2.Controls.Add(Me.num_comida)
        Me.GroupBox2.Location = New System.Drawing.Point(353, 128)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(186, 239)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Acompañaminetos"
        '
        'CB_pelicula
        '
        Me.CB_pelicula.AutoSize = True
        Me.CB_pelicula.Location = New System.Drawing.Point(28, 151)
        Me.CB_pelicula.Name = "CB_pelicula"
        Me.CB_pelicula.Size = New System.Drawing.Size(79, 21)
        Me.CB_pelicula.TabIndex = 11
        Me.CB_pelicula.Text = "Pelicula"
        Me.CB_pelicula.UseVisualStyleBackColor = True
        '
        'CB_bebida
        '
        Me.CB_bebida.AutoSize = True
        Me.CB_bebida.Location = New System.Drawing.Point(28, 97)
        Me.CB_bebida.Name = "CB_bebida"
        Me.CB_bebida.Size = New System.Drawing.Size(74, 21)
        Me.CB_bebida.TabIndex = 10
        Me.CB_bebida.Text = "Bebida"
        Me.CB_bebida.UseVisualStyleBackColor = True
        '
        'CB_comida
        '
        Me.CB_comida.AutoSize = True
        Me.CB_comida.Location = New System.Drawing.Point(28, 41)
        Me.CB_comida.Name = "CB_comida"
        Me.CB_comida.Size = New System.Drawing.Size(77, 21)
        Me.CB_comida.TabIndex = 9
        Me.CB_comida.Text = "Comida"
        Me.CB_comida.UseVisualStyleBackColor = True
        '
        'num_pelicula
        '
        Me.num_pelicula.Location = New System.Drawing.Point(49, 178)
        Me.num_pelicula.Name = "num_pelicula"
        Me.num_pelicula.Size = New System.Drawing.Size(120, 22)
        Me.num_pelicula.TabIndex = 8
        '
        'num_bebida
        '
        Me.num_bebida.Location = New System.Drawing.Point(49, 123)
        Me.num_bebida.Name = "num_bebida"
        Me.num_bebida.Size = New System.Drawing.Size(120, 22)
        Me.num_bebida.TabIndex = 7
        '
        'num_comida
        '
        Me.num_comida.Location = New System.Drawing.Point(49, 68)
        Me.num_comida.Name = "num_comida"
        Me.num_comida.Size = New System.Drawing.Size(120, 22)
        Me.num_comida.TabIndex = 6
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(641, 224)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 39)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "LIMPIAR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(641, 328)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 39)
        Me.Button2.TabIndex = 6
        Me.Button2.Text = "SALIR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(641, 128)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(93, 39)
        Me.Button3.TabIndex = 7
        Me.Button3.Text = "CALCULAR"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(21, 395)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(734, 17)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "*Al adquirir mas de 10 acompañamientos se palica un 10% de descuento automáticame" &
    "nte en el costo por servicio."
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Form1"
        Me.Text = "Venta de Boletos"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.num_pelicula, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.num_bebida, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.num_comida, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents RB_Primera As RadioButton
    Friend WithEvents Label1 As Label
    Friend WithEvents RB_Tercera As RadioButton
    Friend WithEvents RB_Segunda As RadioButton
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents num_pelicula As NumericUpDown
    Friend WithEvents num_bebida As NumericUpDown
    Friend WithEvents num_comida As NumericUpDown
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents CB_pelicula As CheckBox
    Friend WithEvents CB_bebida As CheckBox
    Friend WithEvents CB_comida As CheckBox
    Friend WithEvents Label2 As Label
End Class
