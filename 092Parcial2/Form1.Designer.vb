﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.dg_id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dg_carne = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dg_nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dg_telefono = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dg_nacimiento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dg_genero = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dg_nivel = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AlumnoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me._092Parcial2DataSet = New _092Parcial2._092Parcial2DataSet()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.AlumnoTableAdapter = New _092Parcial2._092Parcial2DataSetTableAdapters.AlumnoTableAdapter()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AlumnoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._092Parcial2DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(20, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Carné"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(261, 31)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(68, 20)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Nombre"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dg_id, Me.dg_carne, Me.dg_nombre, Me.dg_telefono, Me.dg_nacimiento, Me.dg_genero, Me.dg_nivel})
        Me.DataGridView1.Location = New System.Drawing.Point(24, 211)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowHeadersWidth = 51
        Me.DataGridView1.RowTemplate.Height = 28
        Me.DataGridView1.Size = New System.Drawing.Size(1009, 190)
        Me.DataGridView1.TabIndex = 2
        '
        'dg_id
        '
        Me.dg_id.HeaderText = "id"
        Me.dg_id.MinimumWidth = 6
        Me.dg_id.Name = "dg_id"
        Me.dg_id.ReadOnly = True
        Me.dg_id.Width = 125
        '
        'dg_carne
        '
        Me.dg_carne.HeaderText = "carne"
        Me.dg_carne.MinimumWidth = 6
        Me.dg_carne.Name = "dg_carne"
        Me.dg_carne.ReadOnly = True
        Me.dg_carne.Width = 125
        '
        'dg_nombre
        '
        Me.dg_nombre.HeaderText = "nombre"
        Me.dg_nombre.MinimumWidth = 6
        Me.dg_nombre.Name = "dg_nombre"
        Me.dg_nombre.ReadOnly = True
        Me.dg_nombre.Width = 125
        '
        'dg_telefono
        '
        Me.dg_telefono.HeaderText = "telefono"
        Me.dg_telefono.MinimumWidth = 6
        Me.dg_telefono.Name = "dg_telefono"
        Me.dg_telefono.ReadOnly = True
        Me.dg_telefono.Width = 125
        '
        'dg_nacimiento
        '
        Me.dg_nacimiento.HeaderText = "nacimiento"
        Me.dg_nacimiento.MinimumWidth = 6
        Me.dg_nacimiento.Name = "dg_nacimiento"
        Me.dg_nacimiento.ReadOnly = True
        Me.dg_nacimiento.Width = 125
        '
        'dg_genero
        '
        Me.dg_genero.HeaderText = "genero"
        Me.dg_genero.MinimumWidth = 6
        Me.dg_genero.Name = "dg_genero"
        Me.dg_genero.ReadOnly = True
        Me.dg_genero.Width = 125
        '
        'dg_nivel
        '
        Me.dg_nivel.HeaderText = "nivel"
        Me.dg_nivel.MinimumWidth = 6
        Me.dg_nivel.Name = "dg_nivel"
        Me.dg_nivel.ReadOnly = True
        Me.dg_nivel.Width = 125
        '
        'AlumnoBindingSource
        '
        Me.AlumnoBindingSource.DataMember = "Alumno"
        Me.AlumnoBindingSource.DataSource = Me._092Parcial2DataSet
        '
        '_092Parcial2DataSet
        '
        Me._092Parcial2DataSet.DataSetName = "_092Parcial2DataSet"
        Me._092Parcial2DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(265, 73)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(179, 26)
        Me.TextBox1.TabIndex = 4
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(24, 133)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(166, 36)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "Mostrar Datos"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'AlumnoTableAdapter
        '
        Me.AlumnoTableAdapter.ClearBeforeFill = True
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.AlumnoBindingSource
        Me.ComboBox1.DisplayMember = "carnet"
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(24, 73)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(121, 28)
        Me.ComboBox1.TabIndex = 6
        Me.ComboBox1.ValueMember = "carnet"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1110, 450)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AlumnoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._092Parcial2DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents dg_id As DataGridViewTextBoxColumn
    Friend WithEvents dg_carne As DataGridViewTextBoxColumn
    Friend WithEvents dg_nombre As DataGridViewTextBoxColumn
    Friend WithEvents dg_telefono As DataGridViewTextBoxColumn
    Friend WithEvents dg_nacimiento As DataGridViewTextBoxColumn
    Friend WithEvents dg_genero As DataGridViewTextBoxColumn
    Friend WithEvents dg_nivel As DataGridViewTextBoxColumn
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Button1 As Button
    Friend WithEvents _092Parcial2DataSet As _092Parcial2DataSet
    Friend WithEvents AlumnoBindingSource As BindingSource
    Friend WithEvents AlumnoTableAdapter As _092Parcial2DataSetTableAdapters.AlumnoTableAdapter
    Friend WithEvents ComboBox1 As ComboBox
End Class
