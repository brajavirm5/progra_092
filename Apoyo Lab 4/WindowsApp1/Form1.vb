﻿Public Class Form1


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim nombre, apellido, telefono, mayoredad As String
        nombre = nombretext.Text
        apellido = apellidotext.Text
        telefono = telefonotext.Text
        Dim edad As Boolean
        edad = selectedad.Checked()
        If edad = True Then
            mayoredad = "Mayor de edad"
        Else
            mayoredad = "Menor de edad"
        End If

        Dim result As DialogResult = MessageBox.Show("Desea agregar dato", "Titulo", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            Tabla.Rows.Add(New String() {nombre, apellido, telefono, mayoredad})
        Else
            'Codigo en caso de oprimir no
        End If

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        nombretext.Clear()
        apellidotext.Clear()
        telefonotext.Clear()
        Tabla.Rows.Clear()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        NuevoForm.Show()
        Me.Hide()
    End Sub
End Class
