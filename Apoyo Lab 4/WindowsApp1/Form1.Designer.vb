﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.nombretext = New System.Windows.Forms.TextBox()
        Me.apellidotext = New System.Windows.Forms.TextBox()
        Me.telefonotext = New System.Windows.Forms.TextBox()
        Me.selectedad = New System.Windows.Forms.CheckBox()
        Me.Tabla = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        CType(Me.Tabla, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'nombretext
        '
        Me.nombretext.Location = New System.Drawing.Point(43, 38)
        Me.nombretext.Name = "nombretext"
        Me.nombretext.Size = New System.Drawing.Size(100, 20)
        Me.nombretext.TabIndex = 0
        '
        'apellidotext
        '
        Me.apellidotext.Location = New System.Drawing.Point(43, 84)
        Me.apellidotext.Name = "apellidotext"
        Me.apellidotext.Size = New System.Drawing.Size(100, 20)
        Me.apellidotext.TabIndex = 1
        '
        'telefonotext
        '
        Me.telefonotext.Location = New System.Drawing.Point(43, 125)
        Me.telefonotext.Name = "telefonotext"
        Me.telefonotext.Size = New System.Drawing.Size(100, 20)
        Me.telefonotext.TabIndex = 2
        '
        'selectedad
        '
        Me.selectedad.AutoSize = True
        Me.selectedad.Location = New System.Drawing.Point(305, 87)
        Me.selectedad.Name = "selectedad"
        Me.selectedad.Size = New System.Drawing.Size(117, 17)
        Me.selectedad.TabIndex = 3
        Me.selectedad.Text = "Es mayor de edad?"
        Me.selectedad.UseVisualStyleBackColor = True
        '
        'Tabla
        '
        Me.Tabla.AllowUserToAddRows = False
        Me.Tabla.AllowUserToDeleteRows = False
        Me.Tabla.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Tabla.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4})
        Me.Tabla.Location = New System.Drawing.Point(32, 218)
        Me.Tabla.Name = "Tabla"
        Me.Tabla.ReadOnly = True
        Me.Tabla.Size = New System.Drawing.Size(459, 150)
        Me.Tabla.TabIndex = 4
        '
        'Column1
        '
        Me.Column1.HeaderText = "Nombre"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'Column2
        '
        Me.Column2.HeaderText = "Apellido"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        '
        'Column3
        '
        Me.Column3.HeaderText = "Telefono"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        '
        'Column4
        '
        Me.Column4.HeaderText = "Mayor de edad"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(275, 163)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "Guardar Info"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(275, 193)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 6
        Me.Button2.Text = "Limpiar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(53, 189)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(135, 23)
        Me.Button3.TabIndex = 7
        Me.Button3.Text = "Ir a Nuevo Form"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(503, 423)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Tabla)
        Me.Controls.Add(Me.selectedad)
        Me.Controls.Add(Me.telefonotext)
        Me.Controls.Add(Me.apellidotext)
        Me.Controls.Add(Me.nombretext)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.Tabla, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents nombretext As TextBox
    Friend WithEvents apellidotext As TextBox
    Friend WithEvents telefonotext As TextBox
    Friend WithEvents selectedad As CheckBox
    Friend WithEvents Tabla As DataGridView
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Button3 As Button
End Class
