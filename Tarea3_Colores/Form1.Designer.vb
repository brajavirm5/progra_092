﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.az1 = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txt1 = New System.Windows.Forms.TextBox()
        Me.am1 = New System.Windows.Forms.RadioButton()
        Me.ro1 = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txt2 = New System.Windows.Forms.TextBox()
        Me.az2 = New System.Windows.Forms.RadioButton()
        Me.ro2 = New System.Windows.Forms.RadioButton()
        Me.am2 = New System.Windows.Forms.RadioButton()
        Me.but1 = New System.Windows.Forms.Button()
        Me.txtr = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'az1
        '
        Me.az1.AutoSize = True
        Me.az1.ForeColor = System.Drawing.Color.Blue
        Me.az1.Location = New System.Drawing.Point(17, 41)
        Me.az1.Name = "az1"
        Me.az1.Size = New System.Drawing.Size(56, 21)
        Me.az1.TabIndex = 0
        Me.az1.TabStop = True
        Me.az1.Text = "Azul"
        Me.az1.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txt1)
        Me.GroupBox1.Controls.Add(Me.am1)
        Me.GroupBox1.Controls.Add(Me.ro1)
        Me.GroupBox1.Controls.Add(Me.az1)
        Me.GroupBox1.Location = New System.Drawing.Point(58, 35)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(200, 251)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Colores 1"
        '
        'txt1
        '
        Me.txt1.Location = New System.Drawing.Point(26, 211)
        Me.txt1.Name = "txt1"
        Me.txt1.Size = New System.Drawing.Size(151, 22)
        Me.txt1.TabIndex = 5
        '
        'am1
        '
        Me.am1.AutoSize = True
        Me.am1.ForeColor = System.Drawing.Color.Gold
        Me.am1.Location = New System.Drawing.Point(17, 157)
        Me.am1.Name = "am1"
        Me.am1.Size = New System.Drawing.Size(79, 21)
        Me.am1.TabIndex = 2
        Me.am1.TabStop = True
        Me.am1.Text = "Amarillo"
        Me.am1.UseVisualStyleBackColor = True
        '
        'ro1
        '
        Me.ro1.AutoSize = True
        Me.ro1.ForeColor = System.Drawing.Color.Red
        Me.ro1.Location = New System.Drawing.Point(17, 102)
        Me.ro1.Name = "ro1"
        Me.ro1.Size = New System.Drawing.Size(58, 21)
        Me.ro1.TabIndex = 1
        Me.ro1.TabStop = True
        Me.ro1.Text = "Rojo"
        Me.ro1.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txt2)
        Me.GroupBox2.Controls.Add(Me.az2)
        Me.GroupBox2.Controls.Add(Me.ro2)
        Me.GroupBox2.Controls.Add(Me.am2)
        Me.GroupBox2.Location = New System.Drawing.Point(339, 35)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(200, 251)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Colores 2"
        '
        'txt2
        '
        Me.txt2.Location = New System.Drawing.Point(28, 211)
        Me.txt2.Name = "txt2"
        Me.txt2.Size = New System.Drawing.Size(151, 22)
        Me.txt2.TabIndex = 6
        '
        'az2
        '
        Me.az2.AutoSize = True
        Me.az2.ForeColor = System.Drawing.Color.Blue
        Me.az2.Location = New System.Drawing.Point(19, 41)
        Me.az2.Name = "az2"
        Me.az2.Size = New System.Drawing.Size(56, 21)
        Me.az2.TabIndex = 3
        Me.az2.TabStop = True
        Me.az2.Text = "Azul"
        Me.az2.UseVisualStyleBackColor = True
        '
        'ro2
        '
        Me.ro2.AutoSize = True
        Me.ro2.ForeColor = System.Drawing.Color.Red
        Me.ro2.Location = New System.Drawing.Point(17, 93)
        Me.ro2.Name = "ro2"
        Me.ro2.Size = New System.Drawing.Size(58, 21)
        Me.ro2.TabIndex = 3
        Me.ro2.TabStop = True
        Me.ro2.Text = "Rojo"
        Me.ro2.UseVisualStyleBackColor = True
        '
        'am2
        '
        Me.am2.AutoSize = True
        Me.am2.ForeColor = System.Drawing.Color.Gold
        Me.am2.Location = New System.Drawing.Point(17, 157)
        Me.am2.Name = "am2"
        Me.am2.Size = New System.Drawing.Size(79, 21)
        Me.am2.TabIndex = 3
        Me.am2.TabStop = True
        Me.am2.Text = "Amarillo"
        Me.am2.UseVisualStyleBackColor = True
        '
        'but1
        '
        Me.but1.Location = New System.Drawing.Point(339, 304)
        Me.but1.Name = "but1"
        Me.but1.Size = New System.Drawing.Size(151, 34)
        Me.but1.TabIndex = 3
        Me.but1.Text = "MEZCLAR"
        Me.but1.UseVisualStyleBackColor = True
        '
        'txtr
        '
        Me.txtr.Location = New System.Drawing.Point(339, 357)
        Me.txtr.Name = "txtr"
        Me.txtr.Size = New System.Drawing.Size(151, 22)
        Me.txtr.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(30, 313)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(199, 17)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Brandon Javier Ralda Morales"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(149, 343)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 17)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "201603204"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(563, 406)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtr)
        Me.Controls.Add(Me.but1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Form1"
        Me.Text = "Mezclador de colores primarios"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents az1 As RadioButton
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents am1 As RadioButton
    Friend WithEvents ro1 As RadioButton
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents az2 As RadioButton
    Friend WithEvents ro2 As RadioButton
    Friend WithEvents am2 As RadioButton
    Friend WithEvents but1 As Button
    Friend WithEvents txtr As TextBox
    Friend WithEvents txt1 As TextBox
    Friend WithEvents txt2 As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
End Class
